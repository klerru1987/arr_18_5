﻿#include <iostream>
#include <string>

using namespace std;

class Stack
{
private:
    int size=0;
    int* Array = nullptr; //Создаем дополнительный массив чтобы хранить в нем промежуточные данные
public:
    
    void FillArray(int* const arr, const int size) // Соpдаем первоначальный массив с size вводимым пользователем
    {
        for (int i = 0; i < size; i++)
        {
            arr[i] = rand() % 10;
            
        }
        this->size = size; // сохраняем размер массива для доступа внутри класса
        Array = new int[this->size];
        for(int i = 0; i < size; i++)
        {
            Array[i] = arr[i];
            
        }
    }

    void ShowArray(const int* const arr, const int size) // Функция для вывода получившегося массива на экран
    {
        for (int i = 0; i < size; i++)
        {
            cout << arr[i] << "\t";
        }
        cout << endl;
    }

    void pop(int* arr) // Добавляет в массив элемент
    {
        size++;
        arr = new int[size];
        for (int i = 0; i < size; i++)
        {
            if (i < size-1)
               arr[i] = Array[i]; // Копируем из старого массива элементы в новый массив до индекса size-1
            else
               arr[i] = rand() % 10; // Заполняем оставшийся элемент с индексом zise
        }
        
        cout << "Получившийся массив:" << "\t";
        for (int i = 0; i < size; i++)
        {
            cout << arr[i] << "\t";
        }
        cout << endl;

        delete[] Array;  
        Array = nullptr;
        
        Array = new int[size];
        for (int i = 0; i < size; i++)
        {
            Array[i] = arr[i]; // "Очищаем" старый массив и копируем в него значения с нового массива
        }

        delete[] arr;
        arr = nullptr;
        
    }
    void push(int* arr) // Удаляет последний элемент массива
    {
        if (size > 0)
        {
            size--;
            arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                if (i < size)
                    arr[i] = Array[i];
            }
            
            cout  << "Получившийся массив:" << "\t";
            for (int i = 0; i < size; i++)
            {
                cout << arr[i] << "\t";
            }
            cout << endl;

            delete[] Array; 
            Array = nullptr;
            
            Array = new int[size];
            for (int i = 0; i < size; i++)
            {
                Array[i] = arr[i]; // "Очищаем" старый массив и копируем в него значения с нового массива
                
            }

            delete[] arr;
            arr = nullptr;
        }
        else
            cout << endl << "Элемент больше нельзя удалить "<< endl;

    }
};


int main()
{
    setlocale(LC_ALL, "RU");

    int size=0;
    cout << "Введите размер исходного массива - ";
    cin >> size;
    int* MyArray = new int[size];

    Stack NewArray;
    NewArray.FillArray(MyArray, size);
    cout << endl << "Получившийся массив:" << "\t";
    NewArray.ShowArray(MyArray, size);

    delete[] MyArray;
    MyArray = nullptr;
    
    cout << endl << "Введите следующее действие:" << endl;
    cout << "1 - для добавления элемента" << endl;
    cout << "2 - для удаления элемента" << endl;
    cout << "0 - для выхода из программы" << endl;
linc:
    int Case;
    cout << endl;
    cin >> Case;
   
    switch (Case)
        {
        case 1:
        {
            cout << "Добавление элемента \t"<<endl;
            NewArray.pop(MyArray);
            goto linc;
        }
        break;
        case 2:
        {
            cout << "Удаление элемента \t"<<endl;
            NewArray.push(MyArray);
            goto linc;
        }
        break;
        case 0:
        {
            cout << "Выход из ппрограммы";
        }
        break;
        default:
        {
            cout << "Выбрана не верная команда";
            goto linc;
        }
        break;
        }
    
    return 0;
}


